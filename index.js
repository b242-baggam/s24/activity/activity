// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

let number = 2;
let getCube = number ** 3;
console.log(`The cube of ${number} is ${getCube}`);

// 5. Create a variable address with a value of an array containing details of an address.

const address = ['258', 'Washington Ave NW', 'California', '90011'];

// 6. Destructure the array and print out a message with the full address using Template Literals.

console.log(`I live at ${address[0]} ${address[1]}, ${address[2]} ${address[3]}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

let animal = {
	name: 'Lolong',
	type: 'crocodile',
	waterType: 'saltwater',
	weight: 1075,
	measurement: '20 ft 3 in'
};

let {name, type, waterType, weight, measurement} = animal;

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

console.log(`${name} was a ${type} ${waterType} crocodile. He weighed at ${weight} kgs with a measurement of ${measurement}.`)

// 9. Create an array of numbers.

let numbers = [1,2,3,4,5];

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach(function(num){
	console.log(num);
})

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.


const reduceNumber = numbers.reduce((sum, num) => {
	return sum + num;
})
console.log(reduceNumber);


// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

function Dog(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}


// 13. Create/instantiate a new object from the class Dog and console log the object.

const frankie = new Dog('Frankie', 5, 'Miniature Dachshund');
console.log(frankie);